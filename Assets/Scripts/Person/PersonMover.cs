﻿using System;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
  
public class PersonMover : MonoBehaviour
{
    [SerializeField] private MouseLook m_MouseLook; //MouseLook from Standart assets
    private Transform m_Camera_transform;
    private Transform thisTransform;

    bool _isMoving;
    public bool IsMoving
    {
    	get { return _isMoving; }
    	set
    	{
    		_isMoving = value;
    		m_MouseLook.SetCursorLock( value );
    	}
    }

    void Start()
    {
		m_Camera_transform = Camera.main.transform;
		m_MouseLook.Init(transform ,m_Camera_transform);
		thisTransform = transform;
    }

    void Update()
    {
    	if(_isMoving)
			m_MouseLook.LookRotation(thisTransform,m_Camera_transform);
    }
}

