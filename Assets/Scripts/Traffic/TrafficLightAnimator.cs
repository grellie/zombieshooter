﻿using UnityEngine;
using System.Collections;

public class TrafficLightAnimator : MonoBehaviour 
{
	[SerializeField] Material redMaterial;
	[SerializeField] Material greenMaterial;
	[SerializeField] Material defaultMaterial;

	[SerializeField] MeshRenderer redRend;
	[SerializeField] MeshRenderer greenRend;

	[ContextMenu("On")]
	public void On()
	{
		Switch(false);
	}
	[ContextMenu("Off")]
	public void Off()
	{
		Switch(true);
	}
	/// <summary>
	/// Switch the specified off - true for red light
	/// </summary>
	/// <param name="off">If set to <c>true</c> off.</param>
	public void Switch(bool off)
	{
		redRend.material = off? redMaterial : defaultMaterial;
		greenRend.material = off? defaultMaterial : greenMaterial;
	}
	public void Switch(TrafficLight light)
	{
		Switch(light == TrafficLight.Red);
	}
}
