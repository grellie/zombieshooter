﻿using UnityEngine;
using System.Collections;
using System;

public static class ArrayExtension
{
	public static T[] AddPoints<T>(this T[] source, T[] startPoints, T[] endPoints)
	{
		T[] array = new T[source.Length + startPoints.Length + endPoints.Length];
		Array.Copy(startPoints, 0, array, 0, startPoints.Length);
		Array.Copy(source, 0, array, startPoints.Length, source.Length);
		Array.Copy(endPoints, 0, array, startPoints.Length + source.Length, endPoints.Length);
		return array;
	}
}
