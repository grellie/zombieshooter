﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class UIMenu : MonoBehaviour 
{
	#region parameters
	[Header("Menu parameters")]
	[SerializeField] GameObject root;
	[SerializeField] Text header;

	[SerializeField] Button nextLevelButton;
	[SerializeField] Button newGameButton;
	[SerializeField] Button replayButton;
	[SerializeField] Button exitButton;

	[Header("game values parameters")]
	[SerializeField] Text bulletCount;
	[SerializeField] Text lifeCount;
	[SerializeField] Text scoreCount;

	[Header("Cursor")]
	[SerializeField] Image cursorImage;
	#endregion

	#region set
	public void SetGameState(GameState state, int level)
	{
		switch(state)
		{
			case GameState.None:
			Set("New Game!", false, true, false, true);
			break;
			case GameState.PlayLevel: 
			root.SetActive(false);
			cursorImage.gameObject.SetActive(true);
			break;
			case GameState.FailLevel:
			Set("Game Over =(", false, true, level > 1? true : false, true);
			break;
			case GameState.WinLevel: 
			Set("Victory!", true, true, level > 1? true : false, true);
			break;
		}
	}
	public void SetBullet(int bullet, int maxbullet)
	{
		bulletCount.text = bullet.ToString() + "/" + maxbullet.ToString();
	}
	public void SetScore(float score, float maxscore)
	{
		scoreCount.text = score.ToString() + "/" + maxscore.ToString();
	}
	public void SetLife(int life)
	{
		lifeCount.text = life.ToString();
	}

	void Set(string text, bool isNext, bool isNew, bool isReplay, bool isExit)
	{
		header.text = text;
		nextLevelButton.gameObject.SetActive(isNext);
		newGameButton.gameObject.SetActive(isNew);
		replayButton.gameObject.SetActive(isReplay);
		exitButton.gameObject.SetActive(isExit);
		root.SetActive(true);
		cursorImage.gameObject.SetActive(false);
	}

	public void Link(PersonData personData)
	{
		personData.BulletAction += (value) => SetBullet(value, personData.MaxBullet);
		personData.ScoreAction += (value) => SetScore(value, personData.MaxScore);
		personData.LifeAction += SetLife;

		SetBullet(personData.CurrBullet, personData.MaxBullet);
		SetScore(personData.CurrScore, personData.MaxScore);
		SetLife(personData.CurrLife);
	}
	#endregion

	#region events
	public Action NextLevelAction, NewGameAction, ReplayAction, ExitAction;

	void Awake()
	{
		nextLevelButton.onClick.AddListener(() => NextLevelButtonPress());
		newGameButton.onClick.AddListener(() => NewGameButtonPress());
		replayButton.onClick.AddListener(() => ReplayButtonPress());
		exitButton.onClick.AddListener(() => ExitButtonPress());
	}
	void OnDestroy()
	{
		nextLevelButton.onClick.RemoveListener(() => NextLevelButtonPress());
		newGameButton.onClick.RemoveListener(() => NewGameButtonPress());
		replayButton.onClick.RemoveListener(() => ReplayButtonPress());
		exitButton.onClick.RemoveListener(() => ExitButtonPress());
	}
	void NextLevelButtonPress()
	{
		if(NextLevelAction != null) NextLevelAction();
	}
	void NewGameButtonPress()
	{
		if(NewGameAction != null) NewGameAction();
	}
	void ReplayButtonPress()
	{
		if(ReplayAction != null) ReplayAction();
	}
	void ExitButtonPress()
	{
		if(ExitAction != null) ExitAction();
	}
	#endregion

	#if UNITY_EDITOR
	[Header("_testing area")]
	[SerializeField] GameState _testState;
	[SerializeField] int _testLevel;
	[ContextMenu("SetState")]
	public void _SetState()
	{
		SetGameState(_testState, _testLevel);
	}
	#endif

}
