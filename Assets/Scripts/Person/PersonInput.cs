﻿using UnityEngine;
using System.Collections;
using System;

public class PersonInput : MonoBehaviour 
{
	public Action PressShoot;
	public Action PressReload;

	void Update () 
	{
		if(Input.GetKeyDown(KeyCode.Space) && PressShoot != null)
		{
			PressShoot();
		}
		if(Input.GetKeyDown(KeyCode.LeftControl) || Input.GetKeyDown(KeyCode.RightControl))
		{
			if(PressReload != null) PressReload();
		}
	}
}
