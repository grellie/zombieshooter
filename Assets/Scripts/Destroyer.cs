﻿using UnityEngine;
using System.Collections;

public class Destroyer : MonoBehaviour 
{
	[SerializeField] float time;
	void OnCollisionEnter(Collision collision) 
	{
		Invoke("Destroy", time);
    }
    void Destroy()
    {
    	DestroyImmediate(gameObject);
    }
}
