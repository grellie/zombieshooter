﻿using UnityEngine;
using System.Collections;

public class BezierData
{	
	Vector3[] points;
	int curveCount;
	int controlPointCount;
	Vector3 root;

	public BezierData(BezierSpline spline)
	{
		this.points = spline.Points;
		this.curveCount = spline.CurveCount;
		this.controlPointCount = spline.ControlPointCount;
		this.root = spline.transform.position;
	}

	public Vector3 GetPoint (float t) {
		int i;
		if (t >= 1f) {
			t = 1f;
			i = points.Length - 4;
		}
		else {
			t = Mathf.Clamp01(t) * curveCount;
			i = (int)t;
			t -= i;
			i *= 3;
		}
		return root + (Bezier.GetPoint(points[i], points[i + 1], points[i + 2], points[i + 3], t));
	}
	
	public Vector3 GetVelocity (float t) {
		int i;
		if (t >= 1f) {
			t = 1f;
			i = points.Length - 4;
		}
		else {
			t = Mathf.Clamp01(t) * curveCount;
			i = (int)t;
			t -= i;
			i *= 3;
		}
		return Bezier.GetFirstDerivative(points[i], points[i + 1], points[i + 2], points[i + 3], t);
	}
	
	public Vector3 GetDirection (float t) {
		return GetVelocity(t).normalized;
	}

	public void AddPoints(Vector3 startPoint, Vector3 endPoint) 
	{
		Vector3[] starts = new Vector3[3];
		Vector3[] ends = new Vector3[3];

		starts[0] = startPoint - root;
		ends[2] = endPoint - root;

		Vector3 center = (points[0] - starts[0])/2 + starts[0];

		starts[1] = starts[2] = center;

		center = (points[controlPointCount - 1] - ends[2])/4;

		ends[0] = points[controlPointCount - 1] - center + new Vector3(center.x * RValue, center.y, center.z * RValue);
		ends[1] = ends[2] + center + new Vector3(center.x * RValue, center.y, center.z * RValue);
		points = points.AddPoints(starts, ends);

		curveCount += 2;
		controlPointCount += 6;
	}

	/// <summary>
	/// Random value from -1 to 1
	/// </summary>
 	float RValue
	{
		get
		{
			return (-1 + 2 * UnityEngine.Random.value);
		}
	}

}
