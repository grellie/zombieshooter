﻿using UnityEngine;
using System.Collections;
using System;

public enum GameState { None, PlayLevel, WinLevel, FailLevel }

public class Game : MonoBehaviour 
{
	[SerializeField] LevelSetting[] SettingList;
	[SerializeField] RoadController roadController;
	[SerializeField] UIMenu uiMenu;
	[SerializeField] PersonController person;
	[SerializeField] SpawnController spawnController;

	LevelSetting CurrSetting;
	GameState gameState;
	public GameState GameState
	{ 
		get { return gameState; }
		set
		{
			if(gameState == value) return;
			gameState = value;
			switch(value)
			{
				case GameState.None: break;
				case GameState.PlayLevel: StartLevel(CurrLevel); break;
				case GameState.FailLevel: StopLevel(); break;
				case GameState.WinLevel: StopLevel(); break;
			}
			uiMenu.SetGameState(value, CurrLevel);
			person.SetGameState(value, CurrLevel);
		}

	}
	public int CurrLevel { get; private set; }

	void StartLevel(int number)
	{
		CurrSetting = SettingList[number]; //TODO if there is no more levels
		roadController.Run(CurrSetting);
		spawnController.StartSpawning(CurrSetting);
		PersonData data = person.SetSettings(CurrSetting);
		uiMenu.Link(data);
		data.MaxScoreAction += () => { GameState = GameState.WinLevel; };
		data.ZeroLifeAction += () => { GameState = GameState.FailLevel; };
		spawnController.ZombieKilledAction += () => { data.CurrScore += CurrSetting.KillZombieScore; };
		RenderSettings.skybox = CurrSetting.Sky;
	}
	void StopLevel()
	{
		roadController.Stop();
		spawnController.StopSpawning();
	}

	#region menu buttons
	public void StartNewGame()
	{
		CurrLevel = 0;
		GameState = GameState.PlayLevel;
	}
	public void NextLevel()
	{
		CurrLevel ++;
		GameState = GameState.PlayLevel;
	}
	public void ReplayLastLevel()
	{
		GameState = GameState.PlayLevel;
	}
	public void Exit()
	{
		Application.Quit();
	}
	#endregion

	#region UI links / unlinks
	void Awake()
	{
		LinkUI();
	}
	void OnDestroy()
	{
		UnlinkUI();
	}
	void LinkUI()
	{
		uiMenu.NextLevelAction += NextLevel;
		uiMenu.ExitAction += Exit;
		uiMenu.NewGameAction += StartNewGame;
		uiMenu.ReplayAction += ReplayLastLevel;
	}
	void UnlinkUI()
	{
		uiMenu.NextLevelAction -= NextLevel;
		uiMenu.ExitAction -= Exit;
		uiMenu.NewGameAction -= StartNewGame;
		uiMenu.ReplayAction -= ReplayLastLevel;
	}
	#endregion
}