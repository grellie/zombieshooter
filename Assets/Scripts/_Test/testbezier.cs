﻿using UnityEngine;
using System.Collections;

public class testbezier : MonoBehaviour 
{
	public GameObject startpoints;
	public GameObject endpoints;
	public BezierSpline path;

	[ContextMenu("create")]
	public void Create()
	{
		Vector3[] starts = new Vector3[3];
		Vector3[] ends = new Vector3[3];


		starts[0] = startpoints.transform.position - path.transform.position;
		ends[2] = endpoints.transform.position - path.transform.position;

		Vector3 center = (path.GetControlPoint(0) - startpoints.transform.position)/2 + startpoints.transform.position;

		starts[1] = starts[2] = center - path.transform.position;

		center = (path.GetControlPoint(path.ControlPointCount - 1) - endpoints.transform.position)/4;

		ends[0] = path.GetControlPoint(path.ControlPointCount - 1) - center - path.transform.position+ new Vector3(center.x * RValue, center.y, center.z * RValue);
		ends[1] = endpoints.transform.position + center - path.transform.position + new Vector3(center.x * RValue, center.y, center.z * RValue);

//		ends[0] = ends[1] =  center - path.transform.position;

		path.AddPoints(starts, ends);
		BezierControlPointMode[] modes = new BezierControlPointMode[1];
		modes[0] = BezierControlPointMode.Free;
		path.AddNodes(modes, modes);
	}
	public float RValue
	{
		get
		{
			return (-1 + 2 * UnityEngine.Random.value);
		}
	}
}
