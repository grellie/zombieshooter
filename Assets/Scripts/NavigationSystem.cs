﻿using UnityEngine;
using System.Collections;

public class NavigationSystem : MonoBehaviour 
{
	BezierSpline[] pathList;
	void Awake()
	{
		pathList = gameObject.GetComponentsInChildren<BezierSpline>();
	}
	public BezierSpline GetNearest(Vector3 position)
	{
		BezierSpline spline = null;
		float min = float.MaxValue;
		for(int i = 0; i < pathList.Length; i++)
		{
			float length = (pathList[i].transform.position - position).sqrMagnitude;
			if(length < min)
			{
				spline = pathList[i];
				min = length;
			}
		}
		return spline;
	}
}
