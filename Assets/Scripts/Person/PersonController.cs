﻿using UnityEngine;
using System.Collections;

public class PersonController : MonoBehaviour 
{
	PersonInput input;
	PersonMover mover;
	PersonGun gun;

	public PersonData PersonData { get; private set; }

	void Awake()
	{
		input = gameObject.GetComponentInChildren<PersonInput>() as PersonInput;
		mover = gameObject.GetComponentInChildren<PersonMover>() as PersonMover;
		gun = gameObject.GetComponentInChildren<PersonGun>() as PersonGun;
	}

	public void SetGameState(GameState state, int level)
	{
		switch(state)
		{
			case GameState.PlayLevel:
				mover.IsMoving = true;
				input.PressShoot += PressShoot;
				input.PressReload += PressReload;
				break;
			case GameState.None:
			case GameState.WinLevel:
			case GameState.FailLevel:
				mover.IsMoving = false;
				input.PressShoot -= PressShoot;
				input.PressReload -= PressReload;
				break;
		}
	}

	public PersonData SetSettings(LevelSetting settings)
	{
		PersonData = new PersonData(settings);
		gun.SetShootingTime(settings.ShootingTimeDelta);
		return PersonData;
	}

	void PressShoot()
	{
		if(PersonData.CurrBullet > 0 && gun.Shoot())
			PersonData.CurrBullet--;
	}
	void PressReload()
	{
		PersonData.Reload();
	}

	public void AttackedByZombie()
	{
		PersonData.CurrLife--;
	}
}
