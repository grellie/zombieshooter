﻿using UnityEditor;
using UnityEngine;
using System;
using System.Collections.Generic;

[CustomEditor(typeof(Graph))]
public class GraphEditor : Editor
{
   	public override void OnInspectorGUI ()
	{
		GUI.enabled = false;
		base.OnInspectorGUI ();
		GUI.enabled = true;
		Graph Target = target as Graph; 
		if(GUILayout.Button("RecalculateBounds"))
		{
			Target.CalculateObstacles();
		}
	}
    void OnSceneGUI()
    {
		Graph Target = target as Graph; 

		#region upper left ui for X and y count
		Handles.BeginGUI();
		EditorGUILayout.BeginVertical(GUI.skin.box, GUILayout.Width(100));
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("X Count", GUILayout.Width(50));
		Target.xCount = EditorGUILayout.IntField(Target.xCount, GUILayout.Width(50));
		EditorGUILayout.EndHorizontal();
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("Z Count", GUILayout.Width(50));
		Target.zCount = EditorGUILayout.IntField(Target.zCount, GUILayout.Width(50));
		EditorGUILayout.EndHorizontal();
		EditorGUILayout.EndVertical();
        Handles.EndGUI();
        #endregion

        #region start and end point
		Target.startPoint = Handles.PositionHandle(Target.startPoint, Quaternion.identity);
       	Target.endPoint = Handles.PositionHandle(Target.endPoint, Quaternion.identity);
		
       	#endregion

       	#region update if gui changed
		if (GUI.changed)
		{	
			Target.xCount = Mathf.Max(Target.xCount, 2);
        	Target.zCount = Mathf.Max(Target.zCount, 2);
        	Target.nodeSet = new Node[Target.xCount * Target.zCount];

        	float xSize = Target.endPoint.x - Target.startPoint.x;
        	float zSize = Target.endPoint.z - Target.startPoint.z;
        	float xDelta = xSize / (Target.xCount - 1);
        	float zDelta = zSize / (Target.zCount - 1);

        	for(int x = 0; x < Target.xCount; x++)
			{
				for(int z = 0; z < Target.zCount; z++)
				{
					Target.nodeSet[x * Target.zCount + z] = new Node();
					Target.nodeSet[x * Target.zCount + z].Position = Target.startPoint + Vector3.right * xDelta * x + Vector3.forward * zDelta * z;
				}
			}

        }
        #endregion

        #region show nodes
		foreach(Node node in Target.nodeSet)
		{
			Handles.color = node.IsObstacle? Color.red : Color.white;
			Handles.SphereCap(0,node.Position, Quaternion.identity, 0.7f);
		}
		#endregion
    }

}