﻿using UnityEngine;
using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Collections;

public class Graph : MonoBehaviour
{
	[Header("Set all values in SCENE")]
    public Node[] nodeSet;
	public int xCount; 
	public int zCount;
	public Vector3 startPoint;
	public Vector3 endPoint; 


    public string PrintLine() 
    {
    	string line = "Graph\n";
    	for(int i = 0; i < xCount; i ++)
    	{
	    	for(int j = 0; j < zCount; j ++)
	    	{
	    		line += "Node " + nodeSet[i * zCount + j];
			}
	    	line += '\n';
	    }
    	return line;
    }

    #if UNITY_EDITOR
    [ContextMenu("CalculateObstacles")]
    public void CalculateObstacles()
    {
    	BoxCollider[] list = gameObject.GetComponentsInChildren<BoxCollider>() as BoxCollider[];
    	foreach(Node node in nodeSet)
    	{	
			node.IsObstacle = IsObstacle(list, node.Position); 		
    	}
    }
    public bool IsObstacle(BoxCollider[] list, Vector3 position)
    {
		foreach(BoxCollider collider in list)
		{
			if(collider.bounds.Contains(position))
				return true;
		}
		return false;
    }
    #endif
}
[Serializable]
public class Node
{
	public Vector3 Position;
	public bool IsObstacle;
}