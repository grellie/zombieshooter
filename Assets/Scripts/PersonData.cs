﻿using UnityEngine;
using System.Collections;
using System;

//create new persondata each level
public class PersonData 
{
	public Action MaxScoreAction;
	public Action ZeroLifeAction;

	public Action<float> ScoreAction;
	public Action<int> LifeAction;
	public Action<int> BulletAction;

	public float MaxScore { get; private set; }

	float _currScore;
	public float CurrScore
	{
		get { return _currScore; }
		set
		{
			_currScore = value;
			if(ScoreAction != null) ScoreAction(value);
			if(value >= MaxScore && MaxScoreAction != null) MaxScoreAction();
		}
	}

	int _currLife;
	public int CurrLife
	{
		get { return _currLife; }
		set
		{
			_currLife = value;
			if(LifeAction != null) LifeAction(value);
			if(value <= 0 && ZeroLifeAction != null) ZeroLifeAction();
		}
	}

	public int MaxBullet { get; private set; }

	int _currBullet;
	public int CurrBullet
	{
		get { return _currBullet; }
		set
		{
			_currBullet = value;
			if(BulletAction != null) BulletAction(value);
		}
	}

	public void Reload()
	{
		CurrBullet = MaxBullet;
	}

	public PersonData(LevelSetting settings)
	{
		MaxScore = settings.NextLevelScore;
		CurrScore = 0f;
		CurrLife = settings.LifeCount;
		MaxBullet = settings.MaxBulletCount;
		CurrBullet = MaxBullet;
	}
}
