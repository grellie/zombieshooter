﻿using UnityEngine;
using System.Collections;

public class PersonGun : MonoBehaviour 
{
	[SerializeField] GameObject bulletPrefab;//TODO it is better to create object pull instead for bullets (and for zombies too) 
	[SerializeField] GameObject shootPoint; 
	[SerializeField] float power;

	float shootingTimeDelta;
	bool isLocked;

	public void SetShootingTime(float time)
	{
		shootingTimeDelta = time;
	}
	public bool Shoot()
	{
		if(isLocked) return false;

		isLocked = true;

		GameObject bomb = Instantiate(bulletPrefab, shootPoint.transform.position, Quaternion.identity) as GameObject;
		Rigidbody body = bomb.GetComponent<Rigidbody>();
		body.AddForce(shootPoint.transform.forward * power, ForceMode.Impulse);

		Invoke("Unlock", shootingTimeDelta);
		return true;
	}
	void Unlock()
	{
		isLocked = false;
	}
}
