﻿using UnityEngine;
using System.Collections;
using System;

public class LevelSetting : ScriptableObject 
{
	[Header("Player parameters")]
	[SerializeField] float shootingTimeDelta; //minimum time between two shoots
	public float ShootingTimeDelta { get { return shootingTimeDelta; } }

	[SerializeField] int maxBulletCount; 
	public int MaxBulletCount { get { return maxBulletCount; } }

	[SerializeField] int lifeCount;
	public int LifeCount { get { return lifeCount; } }

	[SerializeField] float killZombieScore;
	public float KillZombieScore { get { return killZombieScore; } }

	[SerializeField] float nextLevelScore;
	public float NextLevelScore { get { return nextLevelScore; } }

	[Header("Zombie parameters")]
	[SerializeField] ZombieAreaParameters[] zombieAreaParameterList;
	public ZombieAreaParameters ZombieArea(int areaId)
	{
		for(int i = 0; i < zombieAreaParameterList.Length; i++)
			if(zombieAreaParameterList[i].AreaId == areaId)
				return zombieAreaParameterList[i];
		return null;
	}

	[Header("Road parameters")]
	[SerializeField] TraffingLightParameters[] timingList;
	[SerializeField] float minCarSpeed;
	[SerializeField] float maxCarSpeed;

	[Header("")]
	[SerializeField] Material sky;
	public Material Sky { get { return sky; } }

	public TraffingLightParameters TrafficLight(int id)
	{
		for(int i = 0; i < timingList.Length; i++)
			if(timingList[i].Id == id) return timingList[i];
		return null;
	}
	public float Speed
	{
		get
		{
			return UnityEngine.Random.Range(minCarSpeed, maxCarSpeed);
		}
	}

}
[Serializable]
public class ZombieAreaParameters
{
	[SerializeField] int areaId;
	public int AreaId { get { return areaId; } }

	[SerializeField] float spawnTime;
	public float SpawnTime { get { return spawnTime; } }

	[SerializeField] float attackTime;
	public float AttackTime { get { return attackTime; } }

	[SerializeField] float zombieSpeed;
	public float ZombieSpeed { get { return zombieSpeed; } }

	[SerializeField] bool isDangerous; //if true it will remove one life from player
	public bool IsDangerous { get { return isDangerous; } }
}

[Serializable]
public class TraffingLightParameters
{
	[SerializeField] int id;
	public int Id { get { return id; } }

	[SerializeField] float redTime;
	public float RedTime { get { return redTime; } }

	[SerializeField] float greenTime;
	public float GreenTime { get { return greenTime; } }
}

