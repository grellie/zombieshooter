﻿using UnityEngine;
using System.Collections;
using System;

public class Zombie : MonoBehaviour 
{
	#region parameters
	public Action<Zombie> KilledAction; 

	bool isDead = false;
	
	bool followPath;
	float attackTime;
	float currZombieSpeed;
	float zombieSpeed;
	bool isDangerous; 

	BezierData path;
	Rigidbody body;
	AudioSource audioSource;

	PersonController target;
	#endregion

	#region MonoBehaviour
	void Awake()
	{
		body = gameObject.GetComponent<Rigidbody>();
		audioSource = gameObject.GetComponent<AudioSource>();
	}
	float t = 0;
	void FixedUpdate()
	{
		if(isDead || path == null) return;
		Vector3 velocity = path.GetVelocity(t);
		t += Time.fixedDeltaTime * currZombieSpeed  / velocity.magnitude;
		if(t <= 1)
		{
			body.MovePosition(path.GetPoint(t));
			body.MoveRotation(Quaternion.LookRotation(velocity.normalized));
		} 
		else path = null;
		
	}
	void OnTriggerEnter(Collider other)
	{
		if(other.tag == "SpeedArea")// TODO if happens at the same time with trafficlight 
		{
			zombieSpeed = currZombieSpeed;
	        currZombieSpeed = zombieSpeed * (1 + UnityEngine.Random.value /2);
		}
		if(isDead) return;
		if(other.tag == "PedZone")
		{
			TrafficZone zone = other.gameObject.GetComponent<TrafficZone>();
	        if(zone != null && zone.Type == TrafficType.Pedestrian && zone.Light == TrafficLight.Green)
	        {
	        	zombieSpeed = currZombieSpeed;
	        	currZombieSpeed = 0;
	        	zone.LightAction += LightHandle;
	        }
		}
        else if(other.tag == "Player")
        {
        	path = null;
        	if(isDangerous) 
        	{	
        		target = other.GetComponent<PersonController>();
				StartCoroutine(AttackCoroutine());
        	}
        }
    }
    void OnTriggerExit(Collider other) // TODO if happens at the same time with trafficlight 
    {
		if(other.tag == "SpeedArea")
		{
			currZombieSpeed = zombieSpeed;
		}
    }
	void OnCollisionEnter(Collision collision) 
	{
		if(collision.collider.tag == "Bullet")
		{
			if(!isDead)
			{
				isDead = true;
				StopAllCoroutines();
				if(KilledAction != null) KilledAction(this);
				body.isKinematic = false;
				body.AddForceAtPosition(-collision.impulse * 2, collision.contacts[0].point, ForceMode.Impulse);
				Invoke("Delete", 2);
			}
			else 
			{
				body.AddForceAtPosition(-collision.impulse * 2, collision.contacts[0].point, ForceMode.Impulse);
			}
		}
    }
	#endregion

	#region public 
	public void Set(float attackTime, float zombieSpeed, bool isDangerous)
	{
		this.attackTime = attackTime;
		this.zombieSpeed = zombieSpeed;
		this.currZombieSpeed = zombieSpeed;
		this.isDangerous = isDangerous;
	}
	public void FollowPath(BezierData path)
	{
		this.path = path;
	}
    public void Delete()
    {
    	Destroy(gameObject);
    }
	#endregion

	void LightHandle(TrafficZone zone, TrafficLight type)
    {
    	if(type == TrafficLight.Red)
    	{
    		zone.LightAction -= LightHandle;
    		currZombieSpeed = zombieSpeed;
    	}
    }
    IEnumerator AttackCoroutine()
    {
    	while(true)
    	{
			yield return new WaitForSeconds(attackTime);
			target.AttackedByZombie();
			audioSource.Play();
    	}
    }
}
