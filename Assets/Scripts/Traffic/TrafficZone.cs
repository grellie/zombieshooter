﻿using UnityEngine;
using System.Collections;
using System;

[RequireComponent(typeof(BoxCollider))]
public class TrafficZone : MonoBehaviour 
{
	[SerializeField] TrafficType type;
	public TrafficType Type 
	{
		get { return type; }
	}
	TrafficLight _light;
	public TrafficLight Light
	{
		get { return _light; }
		set 
		{ 	
			if(_light == value) return;
			_light = value; 
			if(LightAction != null) LightAction(this, value);
		}
	}
	public Action<TrafficZone, TrafficLight> LightAction;
}
