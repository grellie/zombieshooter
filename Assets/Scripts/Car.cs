﻿using UnityEngine;
using System.Collections;

public class Car : MonoBehaviour 
{	
	#region public 
	public void Move(float speed, BezierSpline path)
	{
		this.currSpeed = speed;
		this.speed = speed;
		direction = speed > 0 ? 1 : -1;
		this.path = path;
		isMoving = true;
	}
	public void Stop()
	{
		isMoving = false;
	}
	#endregion

	bool isMoving;
	BezierSpline path;
	Rigidbody body;
	float speed; 
	float currSpeed; //while we are stopping currspeed is null, while we are moving currspeed is speed
	int direction;
	float t = 0;

	void Awake()
	{
		body = gameObject.GetComponent<Rigidbody>();
	}
	void FixedUpdate()
	{
		if(!isMoving) return;
		t += Time.fixedDeltaTime * currSpeed  / path.GetVelocity(t).magnitude;
		if(t >= 1) t--; else if(t < 0) t ++;
		body.MovePosition(path.GetPoint(t));
		body.MoveRotation(Quaternion.LookRotation(direction * path.GetDirection(t)));
	}
	void OnTriggerEnter(Collider other) 
	{
        TrafficZone zone = other.gameObject.GetComponent<TrafficZone>();
        if(zone != null && zone.Type == TrafficType.Car && zone.Light == TrafficLight.Red)
        {
        	speed = currSpeed;
        	currSpeed = 0;
        	zone.LightAction += LightHandle;
        }
    }
	void LightHandle(TrafficZone zone, TrafficLight type)
    {
    	if(type == TrafficLight.Green)
    	{
    		zone.LightAction -= LightHandle;
    		currSpeed = speed;
    	}
    }
}