﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
[CustomEditor(typeof(MyLine))]
public class LineEditor : Editor 
{
	Quaternion rotation;
	void OnEnable()
	{
		rotation = Quaternion.LookRotation(Vector3.up);
	}
	void OnSceneGUI()
    {
		MyLine Target = target as MyLine; 

		for(int i = 0; i < Target.list.Count; i++)
		{
			Target.list[i] = Handles.FreeMoveHandle(Target.list[i], rotation, 0.5f, Vector3.zero, Handles.RectangleCap);
		}

		if(GUI.changed){
		List<Vector3> list = Target.list;
		Target.Nodes = new List<Vector3>();
		int end = Target.loop? list.Count : list.Count - 1;
		for(int i = 0; i < end; i ++)
		{
			float length = (list.Loop(i) - list.Loop(i+1)).magnitude * Target.angle;
			int count = (int)((list.Loop(i) - list.Loop(i+1)).magnitude / Target.divider);

			List<Vector3> points = new List<Vector3>(
				Handles.MakeBezierPoints(list.Loop(i), list.Loop(i+1), 
					list.Loop(i) - (list.Loop(i-1) - list.Loop(i+1)).normalized * length , 
					list.Loop(i+1) - (list.Loop(i+2)-list.Loop(i)).normalized * length, count));

//			points.RemoveAt(points.Count - 1);
			Target.Nodes.AddRange(points);
		}
		Target.SetLength();
		}
		Handles.DrawPolyLine(Target.Nodes.ToArray());
    }
   
    public Vector3 Get(Vector3 a, Vector3 b, Vector3 c)
    {
		Vector3 AB = b - a;
		Vector3 C_prime = a + AB / 2;
		return C_prime + Vector3.Normalize(Vector3.Cross(AB, c));
    }

}
