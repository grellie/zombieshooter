﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class SpawnController : MonoBehaviour 
{
	[SerializeField] GameObject zombiePrefab;
	[SerializeField] NavigationSystem navigation;

	SpawnArea[] areaList;
	GameObject root;
	List<Zombie> zombieList = new List<Zombie>();

	public Action ZombieKilledAction;

	void Awake () 
	{
		areaList = gameObject.GetComponentsInChildren<SpawnArea>() as SpawnArea[];
		root = new GameObject("ZombieRoot");
	}
	public void StartSpawning(LevelSetting setting)
	{
		for(int i = 0; i < areaList.Length; i++) 
		{
			ZombieAreaParameters parameters = setting.ZombieArea(areaList[i].Id);
			areaList[i].StartSpawning(parameters.SpawnTime);
			areaList[i].SpawnAtPositionAction += (position) => SpawnAtPositionHandler(position, parameters);
		}
	}
	public void StopSpawning()
	{
		for(int i = 0; i < areaList.Length; i++) 
		{
			areaList[i].StopSpawning();
			areaList[i].SpawnAtPositionAction = null;
		}
		for(int i = 0; i < zombieList.Count; i++)
		{
			zombieList[i].KilledAction -= KilledHandler;
			zombieList[i].Delete();
		}
		zombieList.Clear();
		ZombieKilledAction = null;
	}

	void SpawnAtPositionHandler(Vector3 position, ZombieAreaParameters parameters)
	{
		GameObject zombieObj = (GameObject)Instantiate(zombiePrefab, position, Quaternion.identity);
		zombieObj.transform.SetParent(root.transform);
		Zombie zombie = zombieObj.GetComponent<Zombie>();
		zombie.Set(parameters.AttackTime, parameters.ZombieSpeed, parameters.IsDangerous);

		BezierSpline sourcePath = navigation.GetNearest(zombieObj.transform.position);
		BezierData path = new BezierData(sourcePath);
		path.AddPoints(zombieObj.transform.position, Vector3.zero); //TODO it should be player position, not vector.zero
		zombie.FollowPath(path);

		zombieList.Add(zombie);
		zombie.KilledAction += KilledHandler;
	}
	void KilledHandler(Zombie zombie)
	{
		if(zombieList.Contains(zombie)) 
		{
			zombie.KilledAction -= KilledHandler;
			zombieList.Remove(zombie);
			if(ZombieKilledAction != null) ZombieKilledAction();
		}
	}
}
