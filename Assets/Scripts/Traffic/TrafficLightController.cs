﻿using UnityEngine;
using System.Collections;
using System;

public class TrafficLightController : MonoBehaviour 
{
	[SerializeField] int id;
	public int Id { get { return id; } }

	TrafficLightAnimator animator;
	TrafficZone[] zones;
	float redTiming;
	float greenTiming;

	bool _isWorking;
	public bool IsWorking
	{
		get { return _isWorking; }
		set
		{
			if(_isWorking == value) return;
			_isWorking = value;
			if(value) StartCoroutine(RedCoroutine());
			else StopAllCoroutines();
		}
	}

	void Awake()
	{
		animator = gameObject.GetComponentInChildren<TrafficLightAnimator>() as TrafficLightAnimator;
		zones = gameObject.GetComponentsInChildren<TrafficZone>() as TrafficZone[];
	}

	public void SetTiming(float redTiming, float greenTiming)
	{
		this.redTiming = redTiming;
		this.greenTiming = greenTiming;
	} 

	IEnumerator RedCoroutine()
	{
		SetLightType(TrafficLight.Red);
		yield return new WaitForSeconds(redTiming);
		StartCoroutine(GreenCoroutine());
	}
	IEnumerator GreenCoroutine()
	{
		SetLightType(TrafficLight.Green);
		yield return new WaitForSeconds(greenTiming);
		StartCoroutine(RedCoroutine());
	}
	void SetLightType(TrafficLight light)
	{
		for(int i = 0; i < zones.Length; i++)
		{
			zones[i].Light = light;
		}
		animator.Switch(light);
	}

}
