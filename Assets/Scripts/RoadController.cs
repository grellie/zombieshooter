﻿using UnityEngine;
using System.Collections;

public class RoadController : MonoBehaviour 
{
	TrafficLightController[] lightList;
	BezierSpline[] pathList;
	Car[] carList;

	void Awake () 
	{
		lightList = gameObject.GetComponentsInChildren<TrafficLightController>() as TrafficLightController[];
		pathList = gameObject.GetComponentsInChildren<BezierSpline>() as BezierSpline[];
		carList = gameObject.GetComponentsInChildren<Car>() as Car[];

	}
	public void Run(LevelSetting settings)
	{
		for(int i = 0; i < lightList.Length; i++)
		{
			TraffingLightParameters timing = settings.TrafficLight(lightList[i].Id);
			if(timing != null) 
			{
				lightList[i].SetTiming(timing.RedTime, timing.GreenTime);
				lightList[i].gameObject.SetActive(true);
				lightList[i].IsWorking = true;
			}
			else
			{
				lightList[i].gameObject.SetActive(false);
				lightList[i].IsWorking = false;
			}
		}
		bool reverse = true;//TODO
		for(int i = 0; i < Mathf.Min(pathList.Length, carList.Length); i++)
		{
			carList[i].Move(settings.Speed * (reverse? 1 : -1), pathList[i]);
			reverse = !reverse;
		}
	}
	public void Stop()
	{
		for(int i = 0; i < lightList.Length; i++)
		{
			lightList[i].IsWorking = false;
		}
		for(int i = 0; i < carList.Length; i++)
		{
			carList[i].Stop();
		}
	}

	#if UNITY_EDITOR
	[Header("test parameters")]
	[SerializeField] LevelSetting _testSettings;
	[ContextMenu("Run")]
	void _TestRun()
	{
		Run(_testSettings);
	}
	[ContextMenu("Stop")]
	void _TestStop()
	{
		Stop();
	}
	#endif
}
