﻿using UnityEngine;
using System.Collections;
using System;

public class SpawnArea : MonoBehaviour 
{	
	[SerializeField] int id;
	public int Id { get { return id; } }
	 
	BoxCollider boxCollider;
	Vector3 RandomPointInside
	{
		get
		{
			Vector3 size = boxCollider.bounds.size;
			size.x *= UnityEngine.Random.value;
			size.z *= UnityEngine.Random.value;
			Vector3 point = boxCollider.bounds.min + size;
			point.y = 0;
			return point;
		}
	}

	public Action<Vector3> SpawnAtPositionAction;

	public void StartSpawning(float time)
	{
		StopAllCoroutines();
		StartCoroutine("SpawningCoroutine", time);
	}
	public void StopSpawning()
	{
		StopAllCoroutines();
	}
	IEnumerator SpawningCoroutine(float time)
	{
		while(true)
		{
			yield return new WaitForSeconds(time);
			if(SpawnAtPositionAction != null)
				SpawnAtPositionAction(RandomPointInside);
		}
	}
	void Awake()
	{
		boxCollider = gameObject.GetComponent<BoxCollider>();
	}
}
