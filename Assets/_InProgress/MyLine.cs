﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MyLine : MonoBehaviour 
{
	public bool loop;
	public List<Vector3> list = new List<Vector3>();
	public List<Vector3> Nodes = new List<Vector3>();
	public float angle;
	public int divide;
	public float divider;

	public float Length;

	public void SetLength()
	{
		
		float sum = 0;
		for(int i = 0; i < Nodes.Count - 1; i ++)
		{
			sum += (Nodes[i + 1] - Nodes[i]).magnitude;
		}
		if(loop) sum += (Nodes[0] - Nodes[Nodes.Count - 1]).magnitude;
		Length = sum;
	}
	public float delta;
	public float len;
	public int id;
	public Vector3 Position(float t)
	{
		id = (int) (Nodes.Count * t);
		len = (Nodes.Loop(id + 1) - Nodes.Loop(id)).magnitude;
		delta = ((float)(Nodes.Count * t) - (float)id);
		return Vector3.Lerp(Nodes.Loop(id), Nodes.Loop(id + 1), delta);
	}
}
public static class ListExtension
{
	public static Vector3 Loop(this List<Vector3> list, int index)
	{
		if(index >= list.Count) index -= list.Count;
		else if(index < 0) index += list.Count;
    	return list[index];
	}
}
